export type RootStackParamList = {
  PopularMovies: undefined;
  Search: {searchText: string};
};
