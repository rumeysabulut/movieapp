import React, {useState} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Pressable,
  Modal,
  Text,
} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';

import {RootStackParamList} from '../../RootStackParamList';

const searchIcon = require('../../assets/search.png');

type searchNavigationProps = StackNavigationProp<
  RootStackParamList,
  'PopularMovies'
>;
const SearchBox = () => {
  const navigation = useNavigation<searchNavigationProps>();
  const [searchText, setSearchText] = useState<string>('');
  const [emptyInputAlert, setEmptyInputAlert] = useState<boolean>(false);

  const getEmptyInputAlert = () => (
    <Modal
      animationType="fade"
      transparent={true}
      visible={emptyInputAlert}
      onRequestClose={() => {
        setEmptyInputAlert(!emptyInputAlert);
      }}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>
            {'Enter some characters to search!'}
          </Text>
          <Pressable
            style={styles.button}
            onPress={() => setEmptyInputAlert(!emptyInputAlert)}>
            <Text style={styles.buttonText}>{'OK'}</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );

  const getInput = () => (
    <TextInput
      value={searchText}
      onChangeText={setSearchText}
      placeholder={'Search for a movie'}
      style={styles.input}
    />
  );

  const getPressableIcon = () => (
    <View style={styles.iconContainer}>
      <Pressable
        onPress={() => {
          if (searchText === '') {
            setEmptyInputAlert(true);
          } else {
            navigation.navigate('Search', {
              searchText: searchText,
            });
          }
        }}
        style={styles.iconPressable}>
        <Image style={styles.searchIcon} source={searchIcon} />
      </Pressable>
    </View>
  );

  return (
    <View style={styles.container}>
      {getEmptyInputAlert()}
      {getInput()}
      {getPressableIcon()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
  },
  input: {
    flex: 0.9,
    height: 40,
    borderRadius: 10,
    borderWidth: 1,
    padding: 5,
    fontSize: 18,
    backgroundColor: 'rgb(250, 250, 250)',
  },
  iconContainer: {
    flex: 0.1,
  },
  iconPressable: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchIcon: {
    width: 25,
    height: 25,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 35,
    alignItems: 'center',
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    backgroundColor: 'rgb(243, 144, 33)',
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 16,
  },
});
export default SearchBox;
