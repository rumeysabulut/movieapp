import React from 'react';
import {View, FlatList, ListRenderItemInfo} from 'react-native';

import MovieCard, {MovieCardProps} from '../../components/MovieCard';

type MovieListProps = {
  movieList: MovieCardProps[];
};

const renderMovieItem = (movieCard: ListRenderItemInfo<MovieCardProps>) => (
  <MovieCard {...movieCard.item} />
);

const ListOfPopularMovies = (props: MovieListProps) => (
  <View>
    <FlatList
      data={props.movieList}
      renderItem={renderMovieItem}
      keyExtractor={item => item.id.toString()}
    />
  </View>
);

const PopularMovies = (props: MovieListProps) => {
  return <ListOfPopularMovies movieList={props.movieList} />;
};

export default PopularMovies;
