import React from 'react';
import {View, ActivityIndicator} from 'react-native';

const Loader = () => (
  <View>
    <ActivityIndicator size="large" color={'#999999'} />
  </View>
);
export default Loader;
