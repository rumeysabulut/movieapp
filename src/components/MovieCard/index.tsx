import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export type MovieCardProps = {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
};

const MovieCard = (props: MovieCardProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        {'Title: '}
        <Text style={styles.infoText}>{props.title}</Text>
      </Text>
      <Text style={styles.text}>
        {'Adult: '}
        <Text style={styles.infoText}>{`${props.adult}`}</Text>
      </Text>
      <Text style={styles.text}>
        {'Backdrop Path: '}
        <Text style={styles.infoText}>{props.backdrop_path}</Text>
      </Text>
      <Text style={styles.text}>
        {'Genre IDs: '}
        <Text style={styles.infoText}>{props.genre_ids}</Text>
      </Text>
      <Text style={styles.text}>
        {'ID: '}
        <Text style={styles.infoText}>{props.id}</Text>
      </Text>
      <Text style={styles.text}>
        {'Original Language: '}
        <Text style={styles.infoText}>{props.original_language}</Text>
      </Text>
      <Text style={styles.text}>
        {'Original Title: '}
        <Text style={styles.infoText}>{props.original_title}</Text>
      </Text>
      <Text style={styles.text}>
        {'Overview: '}
        <Text style={styles.infoText}>{props.overview}</Text>
      </Text>
      <Text style={styles.text}>
        {'Popularity: '}
        <Text style={styles.infoText}>{props.popularity}</Text>
      </Text>
      <Text style={styles.text}>
        {'Poster Path: '}
        <Text style={styles.infoText}>{props.poster_path}</Text>
      </Text>
      <Text style={styles.text}>
        {'Release Date: '}
        <Text style={styles.infoText}>{props.release_date}</Text>
      </Text>
      <Text style={styles.text}>
        {'Video: '}
        <Text style={styles.infoText}>{`${props.video}`}</Text>
      </Text>
      <Text style={styles.text}>
        {'Vote Average: '}
        <Text style={styles.infoText}>{props.vote_average}</Text>
      </Text>
      <Text style={styles.text}>
        {'Vote Count: '}
        <Text style={styles.infoText}>{props.vote_count}</Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: 'rgb(182, 80, 0)',
    borderWidth: 1,
    borderRadius: 10,
    margin: 10,
    padding: 10,
    backgroundColor: 'rgb(250, 250, 250)',
  },
  text: {
    fontSize: 18,
  },
  infoText: {
    color: 'rgb(182, 80, 0)',
    fontSize: 18,
  },
});

export default MovieCard;
