import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';

import Loader from '../../components/Loader';
import {MovieCardProps} from '../../components/MovieCard';
import SearchBox from '../../components/SearchBox';
import MovieList from '../../components/MovieList';
import secrets from '../../secrets.json';

const PopularMovies = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [movieList, setMovieList] = useState<MovieCardProps[]>([]);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/movie/popular?api_key=${secrets.API_KEY}`,
      {
        method: 'GET',
      },
    )
      .then(async response => {
        const respJson = await response.json();
        setMovieList(respJson.results);
      })
      .catch(error => console.error(error))
      .finally(() => setIsLoading(false));
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={[styles.container, styles.central]}>
          <Loader />
        </View>
      ) : (
        <View>
          <SearchBox />
          <MovieList movieList={movieList} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  central: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default PopularMovies;
