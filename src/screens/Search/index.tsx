import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {useRoute, RouteProp} from '@react-navigation/native';

import {MovieCardProps} from '../../components/MovieCard';
import MovieList from '../../components/MovieList';
import Loader from '../../components/Loader';
import {RootStackParamList} from '../../RootStackParamList';
import secrets from '../../secrets.json';

type SearchRouteProp = RouteProp<RootStackParamList, 'Search'>;

const Search = () => {
  const route = useRoute<SearchRouteProp>();
  const [isLoading, setIsLoading] = useState(true);
  const [searchResult, setSearchResult] = useState<MovieCardProps[]>([]);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/search/movie?api_key=${secrets.API_KEY}&language=en-US&query=${route.params.searchText}`,
      {
        method: 'GET',
      },
    )
      .then(async response => {
        const res = await response.json();
        setSearchResult(res.results);
      })
      .catch(error => console.error(error))
      .finally(() => setIsLoading(false));
  }, [route.params.searchText]);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={[styles.container, styles.central]}>
          <Loader />
        </View>
      ) : (
        <MovieList movieList={searchResult} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  central: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Search;
