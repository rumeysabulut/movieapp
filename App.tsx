import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import PopularMovies from './src/screens/PopularMovies';
import Search from './src/screens/Search';
import {RootStackParamList} from './src/RootStackParamList';

const Stack = createStackNavigator<RootStackParamList>();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="PopularMovies"
        screenOptions={{
          cardStyle: {backgroundColor: 'white'},
        }}>
        <Stack.Screen
          name="PopularMovies"
          component={PopularMovies}
          options={{
            title: 'Popular Movies',
            headerTitleStyle: {
              fontSize: 20,
            },
          }}
        />
        <Stack.Screen
          name="Search"
          component={Search}
          options={{
            title: 'Search Results',
            headerTitleStyle: {
              fontSize: 20,
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
